<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faisal;
use App\specializedArea;
use DB;

class faisalController extends Controller
{
    public function index()
    {
      $doctor=SpecializedArea::get();

      return view('faisal',['areas'=>$doctor]);
    }
    public function store(Request $request)
    {

      $doctor= new Faisal();
      $doctor->doctorName=$request->doctorName;
      $doctor->specializedArea=$request->specializedArea;
      $doctor->age=$request->age;
      $doctor->experience=$request->experience;
      $doctor->description=$request->description;

      $doctor->save();
      return redirect()->back();
    }

    public function update(Request $request)
    {
      $doctor= Faisal::find($request->id);
      $doctor->doctorName=$request->doctorName;
      $doctor->specializedArea=$request->specializedArea;
      $doctor->age=$request->age;
      $doctor->experience=$request->experience;
      $doctor->description=$request->description;

      $doctor->update();

      return redirect('/show');


    }

    public function edit($id)
    {
      $doctor=Faisal::find($id);
      $specializedAreas=SpecializedArea::get();


      return view('doctorEdit',['doctor'=>$doctor,'specializedAreas'=>$specializedAreas]);

    }

    public function show()
    {

      $doctor=DB::table('faisals')->join('specialized_areas','faisals.specializedArea','specialized_areas.id')
      ->select('faisals.*','specialized_areas.name as specializedArea')->paginate(7);
      return view('doctorsList',['doctors'=>$doctor]);
    }

    public function showSpecializedDoctor($specializedAreas)
    {
      $areaNumber=1;
      if ($specializedAreas=='Anaesthetists') {
        $areaNumber=1;
      }
      elseif ($specializedAreas=='Dermatologists') {
        $areaNumber=2;
      }
      elseif ($specializedAreas=='Haematologists') {
        $areaNumber=3;
      }
      elseif ($specializedAreas=='Gastroenterologists') {
        $areaNumber=4;
      }

      $doctor=DB::table('faisals')->join('specialized_areas','faisals.specializedArea','specialized_areas.id')
      ->select('faisals.*','specialized_areas.name as specializedArea')->where('specializedArea', $areaNumber)->paginate(7);
      return view('doctorsList',['doctors'=>$doctor]);


    }


    public function delete($id)
    {
      $doctor= Faisal::find($id);
      $doctor->delete();
      return redirect('/show');
    }

}
