<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Doctors Finder</title>
    @include('headerlink')
  </head>
  <body>

@include('menu')
<br>
<br>

<form method="post" action="{{ route('faisal.save') }}">

  {!! csrf_field() !!}
  <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Doctor Name</span>
  </div>
  <input type="text" class="form-control" name="doctorName" placeholder="doctor name" aria-label="Username" aria-describedby="basic-addon1">
</div>

<br>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Spesialized Area</span>
  </div>
  <select class="" name="specializedArea">
    @foreach($areas as $area)
    <option value="{{$area -> id}}">{{$area->name}}</option>
    @endforeach
  </select>
</div>
<br>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Age</span>
  </div>
  <input type="number" class="form-control" name="age" placeholder="age" aria-label="Username" aria-describedby="basic-addon1">
</div>
<br>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Experience</span>
  </div>
  <input type="number" class="form-control" name="experience" placeholder="experience year" aria-label="Username" aria-describedby="basic-addon1">
</div>

  <br>
  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="basic-addon1">Description</span>
    </div>
    <input type="text" class="form-control" name="description" placeholder="description" aria-label="Username" aria-describedby="basic-addon1">
  </div>

    <br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
<br><br>










<!-- Footer -->
@include('footer')
<!-- Footer -->


@include('footerlink')

  </body>
</html>
