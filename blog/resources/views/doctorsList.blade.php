<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Doctors Finder</title>
@include('headerlink')
  </head>
  <body>
@include('menu')

<br>
<br>





    <table class="table table-dark">
<thead>
  <tr>
    <th scope="col">ID</th>
    <th scope="col">Doctor's Name</th>
    <th scope="col">Specialized Area</th>
    <th scope="col">Age</th>
    <th scope="col">Experience</th>
    <th scope="col">Description</th>



  </tr>
</thead>

    @foreach ($doctors as $doctor)



<tbody >
  <tr>
    <!-- <th scope="row">1</th> -->
    <td>{{ $doctor->id}}</td>
    <td>{{ $doctor->doctorName}}</td>
    <td>{{ $doctor->specializedArea}}</td>
    <td>{{ $doctor->age}}</td>
    <td>{{ $doctor->experience}} years</td>
    <td>{{ $doctor->description}}</td>

    <td>
        <a class="btn btn-success" href="{{url('/doctor/edit/'.$doctor->id)}}">Edit
</a>
        <a class="btn btn-danger" href="{{url('/doctor/delete/'.$doctor->id)}}" onclick="return confirm('Are you sure?')">Delete
</a>
    </td>



  </tr>

</tbody>


    @endforeach
</table>
{{$doctors->links()}}









@include('footer')

@include('footerlink')


  </body>
</html>
