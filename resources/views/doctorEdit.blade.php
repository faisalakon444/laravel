<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Doctors Finder</title>
    @include('headerlink')
  </head>
  <body>
@include('menu')
<br>
<br>

<form method="post" action="{{ route('faisal.update') }}" name="doctorEditForm">

  {!! csrf_field() !!}
  <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Doctor Name</span>
  </div>
  <input type="hidden" class="form-control" value="{{$doctor->id}}" name="id" >
  <input type="text" class="form-control" name="doctorName" value="{{$doctor->doctorName}}" placeholder="{{$doctor->doctorName}}" aria-label="Username" aria-describedby="basic-addon1">
</div>

<br>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Spesialized Area</span>
  </div>
  <select name="specializedArea">
    @foreach($specializedAreas as $specializedArea)
    <option value="{{$specializedArea -> id}}">{{$specializedArea->name}}</option>
    @endforeach
  </select>
</div>
<br>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Age</span>
  </div>
  <input type="number" class="form-control" name="age" value="{{$doctor->age}}" placeholder="{{$doctor->age}}" aria-label="Username" aria-describedby="basic-addon1">
</div>
<br>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Experience</span>
  </div>
  <input type="number" class="form-control" name="experience" value="{{$doctor->experience}}" placeholder="{{$doctor->experience}}" aria-label="Username" aria-describedby="basic-addon1">
</div>

  <br>
  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="basic-addon1">Description</span>
    </div>
    <input type="text" class="form-control" name="description" value={{$doctor->description}}" placeholder="{{$doctor->description}}" aria-label="Username" aria-describedby="basic-addon1">
  </div>

    <br>
  <button type="submit" class="btn btn-primary">Update</button>
</form>
<br><br>










<!-- Footer -->
@include('footer')
<!-- Footer -->
  <script type="text/javascript">
    document.forms['doctorEditForm'].elements['specializedArea'].value={{$doctor->specializedArea}}
  </script>


  @include('footerlink')

  </body>
</html>
