<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/doctor','faisalController@index')->name('faisal.create');
Route::post('/doctor','faisalController@store')->name('faisal.save');
Route::get('/doctor/show','faisalController@show')->name('faisal.show');
Route::get('/doctor/edit/{id}', 'faisalController@edit')->name('faisal.edit');
Route::get('/doctor/delete/{id}', 'faisalController@delete')->name('faisal.delete');
Route::post('/doctor/update', 'faisalController@update')->name('faisal.update');
Route::get('/doctor/show/{a}', 'faisalController@showSpecializedDoctor')->name('faisal.showSpecializedDoctor');
